/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.loadservice;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import worker_application.QoSManager;

/**
 *
 * @author lise
 */
@WebService(serviceName = "LoadServiceWS")
@Stateless()
public class LoadServiceWS {

    private static final String TASK_QUEUE_NAME = "queue_lise_damien";
    private static final String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com:5672";
    final Connection connection;
    final Channel channel;
    private QoSManager manager;
    
       public LoadServiceWS() throws Exception {
        
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        
        manager = new QoSManager();
        manager.addNewWorker();

        /*
        String message = "10000"; //getMessage(argv);

        channel.basicPublish( "", TASK_QUEUE_NAME, 
                MessageProperties.PERSISTENT_TEXT_PLAIN,
                message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");
*/
        /*
        channel.close();
        connection.close();*/
    }
    
    /**
     * Web service operation
     */
    @WebMethod(operationName = "add")
    public int add(@WebParam(name = "i") int i, @WebParam(name = "j") int j) {
        int k = i + j;
        return k;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "work")
    public String work(@WebParam(name = "n") int n) throws Exception {
        String message = "" + n;
        
        channel.basicPublish( "", TASK_QUEUE_NAME, 
                MessageProperties.PERSISTENT_TEXT_PLAIN,
                message.getBytes());
        manager.incrementNumberMessageSent();
        
        if(manager.getNumberMessageSent() - manager.getNumberTaskDone() - manager.getWorkers().size() > 1){
    	manager.addNewWorker();
    	System.out.println("New Worker created !");
        }
        System.out.println(" [x] Sent '" + message + "'");
        
        return "It's Ok !";
    }
    
}
