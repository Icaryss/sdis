/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loadservicews_client_application;

import java.time.Clock;
import java.util.Scanner;
import com.rabbitmq.client.*;
import java.io.IOException;

/**
 *
 * @author lise
 */
public class LoadServiceWS_Client_Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            String EXCHANGE_NAME = "logs";
            ConnectionFactory factory2 = new ConnectionFactory();
            factory2.setHost("localhost");
            Connection connection2 = factory2.newConnection();
            Channel channel2 = connection2.createChannel();
            
            int n = 0;
            boolean val_ent = false;
            while(val_ent == false) {
                System.out.println("Veuillez choisir une valeur entière");
                Scanner sc = new Scanner(System.in);
                if (sc.hasNextInt()) {
                    n = sc.nextInt();
                    val_ent = true;
                } else {
                    System.out.println("Saisie incorrecte");
                }
            }
            
            final int message = n;
            
            String response = work(message);
            System.out.println("Response = " + response);
            
            channel2.exchangeDeclare(EXCHANGE_NAME, "fanout");
            String queueName = channel2.queueDeclare().getQueue();
            channel2.queueBind(queueName, EXCHANGE_NAME, "");
            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            channel2.basicQos(1);
            
            final Consumer consumer = new DefaultConsumer(channel2) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				String message2 = new String(body, "UTF-8");
                                System.out.println(" [x] Received '" + message2 + "'");
                                
                                if (message == Integer.parseInt(message2)) {
                                    //System.out.println(" [x] Received '" + message2 + "'");     
                                    channel2.basicAck(envelope.getDeliveryTag(), false);
                                    connection2.close();
                                }
                                
				
			}
            };
            channel2.basicConsume(queueName, true, consumer);
            
            
            /*int i = 3;
            int j = 4;
            int result = add(i,j);
            System.out.println("Result = " + result);*/
        } catch (Exception ex) {
            System.out.println("Exception : " + ex);
        }
    }

    private static int add(int i, int j) {
        org.me.loadservice.LoadServiceWS_Service service = new org.me.loadservice.LoadServiceWS_Service();
        org.me.loadservice.LoadServiceWS port = service.getLoadServiceWSPort();
        return port.add(i, j);
    }

    private static String work(int n) {
        org.me.loadservice.LoadServiceWS_Service service = new org.me.loadservice.LoadServiceWS_Service();
        org.me.loadservice.LoadServiceWS port = service.getLoadServiceWSPort();
        return port.work(n);
    }
    
}
