import java.util.ArrayList;


public class QoSManager {
	
	private int numberMessageInQueue;
	private int numberTaskDone;
	private int numberMessageSent;
	private ArrayList<Worker> workers;
	
	public QoSManager() {
		super();
		this.numberMessageInQueue = 0;
		setNumberTaskDone(0);
		setNumberMessageSent(0);
		this.workers = new ArrayList<Worker>();
	}

	public int getNumberMessageInQueue() {
		return numberMessageInQueue;
	}

	public void incrementNumberMessageSent() {
		this.setNumberMessageSent(this.getNumberMessageSent() + 1);
	}
	
	public void incrementNumberTaskDone() {
		this.setNumberTaskDone(this.getNumberTaskDone() + 1);
	}

	public ArrayList<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(ArrayList<Worker> workers) {
		this.workers = workers;
	}
	
	public void addNewWorker() throws Exception{
		Worker w = new Worker(this);
		w.exec();
		this.workers.add(w);
	}
	
	public void removeWorker(Worker w){
		workers.remove(w);
	}

	public int getNumberMessageSent() {
		return numberMessageSent;
	}

	public void setNumberMessageSent(int numberMessageSent) {
		this.numberMessageSent = numberMessageSent;
	}

	public int getNumberTaskDone() {
		return numberTaskDone;
	}

	public void setNumberTaskDone(int numberTaskDone) {
		this.numberTaskDone = numberTaskDone;
	}
	
	

}
