/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker_application;

import com.rabbitmq.client.*;
import java.io.IOException;

/**
 *
 * @author lise
 */
public class Worker {
    private static final String TASK_QUEUE_NAME = "queue_lise_damien";
    private static final String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com:5672";
    
    final private QoSManager manager;
    
    final Connection connection;
    final Channel channel;
    
    private static final String EXCHANGE_NAME = "logs";
    final Connection connection2;
    final Channel channel2;
    
    public Worker(QoSManager m) throws Exception {
		super();
		manager = m;
                
                ConnectionFactory factory = new ConnectionFactory();
                connection = factory.newConnection();
		channel = connection.createChannel();
                factory.setHost("localhost");
                
                ConnectionFactory factory2 = new ConnectionFactory();
                connection2 = factory2.newConnection();
                channel2 = connection2.createChannel();
                factory2.setHost("localhost");
	}
        
    public void exec() throws Exception {
                
		channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		channel.basicQos(1);

      		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				String message = new String(body, "UTF-8");

				System.out.println(" [x] Received '" + message + "'");
				try {
					doWork(Integer.parseInt(message));
                                       
				} finally {
                                        try {
					System.out.println(" [x] Done");
                                        channel2.exchangeDeclare(EXCHANGE_NAME, "fanout");
                                        channel2.basicPublish(EXCHANGE_NAME, "", null,
                                        message.getBytes());
                                        System.out.println(" [x] Sent '" + message + "'");
                                        
                                        /*channel2.close();
                                        connection2.close();*/
                                        
					channel.basicAck(envelope.getDeliveryTag(), false);
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
				}
			}
		};
		channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
    }

	private static void doWork(int time) {
		
			System.out.println("Let's work !");
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
                        
			System.out.println("Fini !");
		

	}
    
}
