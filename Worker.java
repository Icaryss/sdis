import com.rabbitmq.client.*;
import java.io.IOException;

public class Worker {
	private static final String TASK_QUEUE_NAME = "queue_lise_damien";
	private static final String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com:5672";
	
	private QoSManager manager;

	public Worker(QoSManager m) {
		super();
		manager = m;
	}

	public void exec() throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		final Connection connection = factory.newConnection();
		final Channel channel = connection.createChannel();

		channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		channel.basicQos(1);

		final Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
				String message = new String(body, "UTF-8");

				System.out.println(" [x] Received '" + message + "'");
				try {
					doWork(Integer.parseInt(message));
				} finally {
					manager.incrementNumberTaskDone();
					System.out.println(" [x] Done");
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}
		};
		
		channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
	}

	private static void doWork(int time) {
		
			System.out.println("Let's work !");
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Fini !");
		

	}
}