import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

public class LoadService {

  private static final String TASK_QUEUE_NAME = "queue_lise_damien";
  private static final String HOST = "ec2-54-157-2-56.compute-1.amazonaws.com:5672";

  public static void main(String[] argv) 
                      throws Exception {

    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    final Connection connection = factory.newConnection();
	final Channel channel = connection.createChannel();

    channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
    
    QoSManager manager = new QoSManager();
    manager.addNewWorker();

    String message = "10000"; //getMessage(argv);

    channel.basicPublish( "", TASK_QUEUE_NAME, 
            MessageProperties.PERSISTENT_TEXT_PLAIN,
            message.getBytes());
    manager.incrementNumberMessageSent();
    
    if(manager.getNumberMessageSent() - manager.getNumberTaskDone() - manager.getWorkers().size() > 1){
    	manager.addNewWorker();
    	System.out.println("New Worker created !");
    }
    
    System.out.println(" [x] Sent '" + message + "'");

    channel.close();
    connection.close();
  }      
  //...
}